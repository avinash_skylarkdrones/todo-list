import { type Uid } from '@/types/Uid'
import { type Todo } from '@/types/Todo'

export type TodoList = Record<Uid, Todo>
