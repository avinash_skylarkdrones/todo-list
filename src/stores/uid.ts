import { ref } from 'vue'
import { defineStore } from 'pinia'

import { type Uid } from '@/types/Uid'

export const useUidStore = defineStore('uid', () => {
  const uid = ref(0 as Uid)
  function next() {
    uid.value++
  }

  return { uid, next }
})
