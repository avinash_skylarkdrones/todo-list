import { computed, ref } from 'vue'
import { defineStore } from 'pinia'

import { type Uid } from '@/types/Uid'
import { type Todo } from '@/types/Todo'
import { type TodoList } from '@/types/TodoList'

import { useUidStore } from '@/stores/uid'

const uidStore = useUidStore()
export const useTodoListStore = defineStore('todoList', () => {
  const todoList = ref({} as TodoList)
  const completedTodos = computed(() =>
    Object.fromEntries(Object.entries(todoList.value).filter(([, { completed }]) => completed))
  )
  const incompleteTodos = computed(() =>
    Object.fromEntries(Object.entries(todoList.value).filter(([, { completed }]) => !completed))
  )

  function createTodo(title: string): Uid {
    const id = uidStore.uid
    const completed = false
    const todo = { title, completed } as Todo

    todoList.value[id] = todo
    uidStore.next()

    return id
  }

  function getCompletionStatus(id: Uid): boolean {
    return todoList.value[id].completed
  }

  function setCompletionStatus(id: Uid, completed: boolean) {
    todoList.value[id].completed = completed
  }

  function removeTodo(id: Uid) {
    delete todoList.value[id]
  }

  return {
    todoList,
    completedTodos,
    incompleteTodos,
    createTodo,
    getCompletionStatus,
    setCompletionStatus,
    removeTodo
  }
})
