import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'create',
      component: () => import('@/views/CreateView.vue')
    },
    {
      path: '/delete',
      name: 'delete',
      // route level code-splitting
      // this generates a separate chunk (Delete.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('@/views/DeleteView.vue')
    }
  ]
})

export default router
